### Resto catalog deployment using helm charts.

Implemented based on [this repo](https://github.com/jjrom/resto)

We used kompose -c to convert the existing yaml to helm charts.

## Tweaks

We had to create a config map for the postgresql, because we had a issue with the docker not being able to map the existing config file into the container, so we created the configmap using `kubectl create configmap postgres-configmap.yaml  --from-file  postgresql.conf` 

We also had to map the configmap within `restodb-deployment.yaml` file:

```
...
volumeMounts:
    - mountPath: /etc/postgresql.conf
        name: postgres-conf


...

volumes:
    - name: postgres-conf
    configMap:
        name: postgres-configmap
            
...

```

We also had to setup postgres to be able to know where the config file was located at, so on the  `restodb-deployment.yaml` file we also added the following command:

```
...
exec:
    command:
        - postgres -c  config_file=/etc/postgresql.conf && pg_isready -U resto
...
```

We also removed all network policies [the original repo had](https://github.com/jjrom/resto)


We altered some values within the `config-env-configmap.yaml` file, mainly the following: 


```
DATABASE_HOST: restodb.resto.svc.cluster.local

PUBLIC_ENDPOINT: ENDPOINTURL

RESTO_EXPOSED_PORT: "8080"

```

The `DATABASE_HOST` had to be changed because it was set to `resto` and the pods were unable to resolve that host name, so we changed it to the name of the pod runnig the service.

The `PUBLIC_ENDPOINT` had also to be altered so that the service would be redirected to the correct url.

The `RESTO_EXPOSED_PORT` was changed because that is the correct port where the service is listening on.


In the `resto-service.yaml` file we had to alter the ports to the correct ones:

```
spec:
  ports:
    - name: "8080"
      port: 8080
      targetPort: 80
```

## Deploying the service

To deploy the service on needs to create a namespace:

`helm install resto . --create-namespace -n resto`

The service will create two pods and we then need to create the ingress:

`kubectl apply -f `



