Setup the repository:
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```
Get the helm chart values
```
helm show values ingress-nginx/ingress-nginx > values.yaml
```

Edit the values.yaml file accordingly

``` yaml
...

#type: LoadBalancer

    type: NodePort
    nodePorts:
      http: 80
      https: 443
      tcp:
        8080: 8080
...
```

Setup kubernetes service ports range at `/etc/kubernetes/manifests/kube-apiserver.yaml`
```
...
- --service-node-port-range=1-32767
...
```
#### NOTE:
We had a few issues regarding the exposure of the ports for the service and we found [this solution](https://stackoverflow.com/questions/58574104/kubernetes-nginx-ingress-controller-on-nodeport/58732307#58732307)



Install the nginx chart wiht the costumizations we made on the `values.yaml` file
```
helm install ingress-nginx --namespace ingress-nginx ingress-nginx/ingress-nginx --create-namespace -f values.yaml --version=4.2.5
```
