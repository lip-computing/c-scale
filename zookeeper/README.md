Manual command to setup Zookeeper in kubernetes

```
helm repo add bitname/zookeeper https://charts.bitnami.com/bitnami

helm install zookeeper-cscale bitnami/zookeeper -f zookeeper/values.yaml --create-namespace -n zookeeper

```
