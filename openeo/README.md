Add repos:
```
helm repo add spark-operator https://googlecloudplatform.github.io/spark-on-k8s-operator'
helm repo add sparkapp https://artifactory.vgt.vito.be/helm-charts'
```


Create namespaces:
```
kubectl create namespace spark-jobs'
kubectl create namespace spark-history'
```

Create secrets:
```
kubectl -n spark-jobs create secret tls openeocert --cert=your_certs_path/openeo_a_incd_pt.cer.reversed --key=your_certs_path/openeo.a.incd.pt.key'
kubectl -n spark-history create secret tls openeocert --cert=your_certs_path/openeo_a_incd_pt.cer.reversed --key=your_certs_path/openeo.a.incd.pt.key'
```

Create and apply configmaps:
```
kubectl apply -f your_repo_path/openeo/layercatalogjson.yaml -n spark-jobs'
kubectl apply -f your_repo_path/openeo/sparkapplicationyaml.yaml -n spark-jobs'
kubectl apply -f your_repo_path/openeo/http-credentials.yaml -n spark-jobs'
```

Install spark-operator
```
helm install spark-operator/spark-operator --generate-name --create-namespace --namespace spark-operator --set sparkJobNamespace=spark-jobs --set webhook.enable=true'
```

Install spark-application:
```
helm install myspark sparkapp/sparkapplication --namespace spark-jobs -f your_repo_path/openeo/values.yaml'
```

Install spark-history:
```
helm install mysparkhistory sparkapp/spark-history-server -f your_repo_path/openeo/shs_values.yaml --namespace spark-history --version 0.0.1'
```

Apply ingresses:
```
kubectl apply -f your_repo_path/openeo/ingress.yaml -n spark-jobs'
kubectl apply -f your_repo_path/openeo/shs_ingress.yaml -n spark-history'
kubectl apply -f your_repo_path/openeo/jobtracker.yaml -n spark-jobs'
```