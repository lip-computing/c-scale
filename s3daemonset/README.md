# s3daemonset

Based on [this post](https://blog.meain.io/2020/mounting-s3-bucket-kube/)

### We created our own image

We [created an image](https://hub.docker.com/repository/docker/zbenta/s3fs) with Alpine Linux with s3fs version 1.91 installed, that can be used to mount an s3 bucket to the /var/s3fs directory.

it has the following environment variables:

    S3_REGION
    S3_BUCKET
    AWS_KEY
    AWS_SECRET_KEY
    AWS_ENDPOINT

This can be used in a deamonset to allow for the mapping of a s3 bucket in all the nodes belonging to a kubernetes cluster.

Here is the `Dockerfile`:

```
# Docker file with s3fs v1.91 and alpine latest
# Authors: Zacarias Benta - zacarias@lip.pt
#          César Ferreira- cesar@lip.pt

FROM alpine:latest

ENV MNT_POINT /var/s3fs
ARG S3FS_VERSION=v1.91

RUN apk --update --no-cache add fuse alpine-sdk automake autoconf libxml2-dev fuse-dev curl-dev git bash; \
    git clone https://github.com/s3fs-fuse/s3fs-fuse.git; \
    cd s3fs-fuse; \
    git checkout tags/${S3FS_VERSION}; \
    ./autogen.sh; \
    ./configure --prefix=/usr; \
    make; \
    make install; \
    make clean; \
    rm -rf /var/cache/apk/*; \
    apk del git automake autoconf;

RUN mkdir -p "$MNT_POINT"

COPY run.sh run.sh
CMD ./run.sh

```

And here is the `run.sh` file:

```
echo "$AWS_KEY:$AWS_SECRET_KEY" > passwd && chmod 600 passwd
s3fs "$S3_BUCKET" "$MNT_POINT" -o passwd_file=passwd -o url="$AWS_ENDPOINT" -o use_path_request_style && tail -f /dev/null
```

### Create service to mount s3 bucket on all kubernetes nodes

We need to have the bucket we wish to use on all of the kubernetes nodes, for that we created a systemd service to mount the desired bucket as follows:

```
Description=S3FS FUSE mount
Wants=network-online.target
After=network-online.target
AssertPathIsDirectory=/mnt/s3data

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/bin/s3fs BUCKET_NAME /mnt/s3data -o passwd_file=/root/passwd -o url=https://S3_ENDPOINT:S3_ENDPOINT_PORT -o use_path_request_style -o allow_other -o nonempty
ExecStop=/bin/fusermount -u /mnt/s3data

[Install]
WantedBy=default.target
```
#### Note: You must create the passwd file with the command `echo "$AWS_KEY:$AWS_SECRET_KEY" > passwd && chmod 600 passwd`


### We created a configmap

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: s3-config
  namespace: s3test
data:
  S3_REGION: "RegionOne"
  S3_BUCKET: "BUCKET_NAME"
  AWS_KEY: "somekey"
  AWS_SECRET_KEY: "somesecret"
  AWS_ENDPOINT: "AWS_ENDPOINT"

```

Applied it:

```
kubectl apply -f configmap.yaml -n s3daemonset
```

### we created a daemonset

A deamonset is used to execute the container we have defined previously in every pod belongin to the kubernetes cluste, this way we are sure that the s3fs moutpoint is available in every node.

```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    app: s3-provider
  name: s3-provider
  namespace: s3test
spec:
  selector:
    matchLabels:
      app: s3-provider
  template:
    metadata:
      labels:
        app: s3-provider
    spec:
      containers:
      - name: s3fuse
        image: zbenta/s3fs:v1.91
        securityContext:
          privileged: true
        envFrom:
        - configMapRef:
            name: s3-config
        volumeMounts:
        - name: devfuse
          mountPath: /dev/fuse
        - name: mntdatas3fs
          mountPath: /var/s3fs:shared
      volumes:
      - name: devfuse
        hostPath:
          path: /dev/fuse
      - name: mntdatas3fs
        hostPath:
          path: /mnt/s3data
```
Apply the daemonset:

```
k apply -f daemonset.yaml -n s3daemonset
```

To make sure all is runnig just check the following:

```
[root@K8S_CLUSTER s3daemonset]# kubectl get all -n s3daemonset
NAME                    READY   STATUS    RESTARTS   AGE
pod/s3-provider-2897n   1/1     Running   0          33d
pod/s3-provider-2j62g   1/1     Running   0          33d
pod/s3-provider-84mfl   1/1     Running   6          33d
pod/s3-provider-s4lmg   1/1     Running   0          33d
pod/s3-provider-vztxl   1/1     Running   0          33d
pod/test-pd             1/1     Running   0          33d

NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/s3-provider   5         5         5       5            5           <none>          33d

```

### Create a test pod to make sure all is working as intended

pod.yaml:

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
  namespace: s3test
spec:
  containers:
  - image: nginx
    name: s3-test-container
    securityContext:
      privileged: true
    volumeMounts:
    - name: mntdatas3fs
      mountPath: /var/s3fs:shared
  volumes:
  - name: mntdatas3fs
    hostPath:
      path: /mnt/s3data
```

Run the pod and make sure the mount point exists:

```
kubectl apply -f pod.yaml -n s3daemonset
```
See if the mountpoint exists on the pod:

```
root@test-pd:/# df -h
Filesystem      Size  Used Avail Use% Mounted on
overlay         500G  5.2G  495G   2% /
tmpfs            64M     0   64M   0% /dev
tmpfs            16G     0   16G   0% /sys/fs/cgroup
/dev/sda1       500G  5.2G  495G   2% /etc/hosts
s3fs             16E     0   16E   0% /var/s3fs
shm              64M     0   64M   0% /dev/shm
tmpfs            16G   12K   16G   1% /run/secrets/kubernetes.io/serviceaccount
root@test-pd:/# 
```

