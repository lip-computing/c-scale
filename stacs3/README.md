# Create namespace

```
kubectl create ns stacs3
```


# Create ConfigMap for eodag.yaml and providers.yam

```
kubectl apply -f stac-server-configmap.yaml
kubectl apply -f stac-server-providers-configmap.yaml
```

# Deploy STAC-SERVER PODS

```
kubectl apply -f stac-server-deployment.yaml
```

We could check logs during POD deployment by typing:

```
kubectl logs $(kubectl get pods -n stacs3  --no-headers -o custom-columns=":metadata.name") -n stacs3 -f
```

# Deploy STAC-SERVER Service

```
kubectl apply -f stac-server-service.yaml
```

# Generate Secret with SSL certificates for STAC ingress

```
sh generate_secrets
```

# Deploy STAC-SERVER Ingress

```
kubectl apply -f stac-server-ingress.yaml
```


# Download products from CREODIAS

To download products from CREODIAS you need to execute the `get_products.py` script, the products will be dowloaded and extracted to the S3_BUCKET you have defined in the configuration files of the stacs3 server.

# Register catalog on CESNET

To register the downloaded products use the `register_products.py` script, please fill in the username and password accordingly.