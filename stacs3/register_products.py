#################################################
#                                               #
#               LIP / INCD 2022                 #
#                                               #
#   Authors:                                    #
#           Zacarias Benta - zacarias@lip.pt    # 
#           César Ferreira - cesar@lip.pt       #
#                                               #
#################################################

from copy import deepcopy
from email.mime import base
import json
from xml.dom.expatbuilder import parseString
import xml.etree.ElementTree as ET
from xml.dom import minidom
import traceback
import urllib3
import xmltodict
import requests
import pycurl
from io import StringIO
from pystac_client  import Client
from re import I
from bs4 import BeautifulSoup
import urllib.request
import os
import time

# Collect product metadata from another provider. In our case it is the following:
api = Client.open('https://earth-search.aws.element84.com/v0')

# define the spatial data to collect
geom = {}
# Geometry coordinates for the area to search
# Example for the south area of Iberian Peninsula
# geom =  {
#   "type": "Polygon",
#   "coordinates": [
#     [
#       [
#         -7.682155041704681,
#         38.620982842287496
#       ],
#       [
#         -7.682155041704681,
#         36.18203953636458
#       ],
#       [
#         -5.083888440142181,
#         36.18203953636458
#       ],
#       [
#         -5.083888440142181,
#         38.620982842287496
#       ],
#       [
#         -7.682155041704681,
#         38.620982842287496
#       ]
#     ]
#   ]
# }

# concatenate the spatial and temporal data and collections data to be queried
search = api.search(
  max_items=None,
  collections = "COLLECTION_TO_QUERY", # Example: "sentinel-s2-l1c"
  intersects = geom,
  datetime = "START_DATE/END_DATE" # Date format: yyyy-mm-dd
)

#get the nr of products matched
values=search.matched()

#create an products dictionary
itemdict=search.get_all_items_as_dict()
items = list(search.items_as_dicts())

# list of existing files within S3 bucket
existingfiles = []
data_dir = "DATA_DIRECTORY" # Directory with products previously download and ready to be registered
for x in os.listdir(data_dir):
    if x.endswith(".SAFE"):
      x = x[:-5]
      existingfiles.append(x)

total_products = len(existingfiles)
print("\n",total_products, " products ready to be registered!")


#function to get metadata from within a XML file passsed as an url
# args:
#   url - url where the xml file is stored
#   object - the xml object you wish to locate within the xml file
#   tag - the tag within the xml object you wish to locate
#   id - the identification of the value within the tag you wish to locate
# returns:
#   metadata within the xml file

def getMTD(url,object, tag, id):
	result=[]
	req = urllib.request.urlopen(url)
	xml = BeautifulSoup(req,'xml')
	if tag!="":
		for item in xml.find_all(object,{tag:id}):
			#print(item)
			for data in item:
				#print("DATA:", data)
				for meta in data:
					#print("META:",meta)
					strmeta=str(meta)
					if 'href' in strmeta :
						#print("STRMETA",strmeta)
						result.append(strmeta)
	elif tag=="":
		for item in xml.find_all(object):
			#print(item)
			for data in item:
				stritem=str(item)
				if id in  stritem:#print("DATA:", data)
					for meta in data:
						#print("META:",meta)
						strmeta=str(meta)
						if 'href' in strmeta :
							#print("STRMETA",strmeta)
							result.append(strmeta)
	return result


# function to retrieve the products metadata from https://earth-search.aws.element84.com/v0
# args:
#   items - item dictionary with all the products available for the specified spatial and temporal coordenates
#   filename - the product name we wish to locate within the dictionary
# returns:
#   the json data regarding the specified product                     

def getmetadata(items,filename):
  for i in items:
    prop=i['properties']
    for p in prop:
      if p == 'sentinel:product_id':
        if i['properties'][p] == filename:
          jsonitem=json.dumps(i, indent = 4)
          # print(jsonitem)
          jsonitem=json.loads(jsonitem)
          return jsonitem
  return ""

# function to retrieve the products id in catalogue of products that are already registered and need to be updated
# args:
#   items - item dictionary with all the products available for the specified spatial and temporal coordenates
#   filename - the product name we wish to find the product id
# returns: 

def get_id(items,filename):
  for i in items:
    prop=i['properties']
    for p in prop:
      if p == 'sentinel:product_id':
        if i['properties'][p] == filename:
          update_id=i['id']
          return update_id
  return ""


#function used to replace the metadata that we get from https://earth-search.aws.element84.com/v0
# and add the one we heve in our files
# args:
#   metadata - metadata in json format, extracted from https://earth-search.aws.element84.com/v0
#   identifier - the product identifier, for which we wish to replace the metadata
# returns:
#   a json file with all the metadata correctly pointing to S3 bucket

def replacementadata(metadata,identifier):
  jsonitem=metadata
  identifiersafe=identifier
  identifiernosafe=identifier[:-5]
  baseurl="URL_FOR_S3_BUCKET/BUCKET_NAME/DATA_DIR"
  dataurl=baseurl+identifiersafe+"/"+identifiersafe+"/"
  manifestexists=False
  for x in os.listdir(data_dir):
    if x == "manifest.safe":
      manifestexists=True
  
  manifesturl=dataurl+"manifest.safe"
  #change collection name
  jsonitem['collection']="S2"
  #thumbnail
  thumbnail=dataurl+identifiernosafe+"-ql.jpg"
  jsonitem['assets']["thumbnail"]["href"]=thumbnail
  #overview tciimage
  if manifestexists is True:
    tciimage=getMTD(manifesturl,"dataObject","ID","IMG_DATA_Band_TCI_Tile1_Data")
    tciimagepath=dataurl+tciimage[0][22:-21]
    jsonitem['assets']["overview"]["href"]=tciimagepath  
  #metadata
    meta_data=getMTD(manifesturl,"dataObject","ID","S2_Level-1C_Tile1_Metadata")
    metadatapath=dataurl+meta_data[0][22:-21]
    jsonitem['assets']["metadata"]["href"]=metadatapath
  #info and links needs to be popped because we have no such information in our files
    jsonitem['assets'].pop('info')
    jsonitem.pop('links')
  #images
    imagelist=getMTD(manifesturl,"dataObject","","IMG_DATA_Band_")
  #loop through existing images
    k=0
    for i in range(1,13):
      imagename=""
      if i<10:
        imagename="B0"+str(i)
      else:
        imagename="B"+str(i)
      jsonitem['assets'][imagename]["href"]=dataurl+imagelist[k][22:-21]
      k+=1
  return jsonitem

# function used to register metadata at cesnet
# args:
#   cesneturl - the url to the stact catalog
#   json_data - the metadata we wish to register
# returns:
#   status code of the post request
# NOTE: please add the correct username and password to authenticate at cesnet

def registercesnet(cesneturl,json_data):
  result=""
  if json_data !="":
    body_as_json_string = json.dumps(json_data) # dict to json
    body_as_file_object = StringIO(body_as_json_string)
    curl = pycurl.Curl()
    curl.setopt(pycurl.URL, cesneturl)
    curl.setopt(pycurl.HTTPHEADER, ['Accept: application/json','Content-Type: application/json'])
    curl.setopt(pycurl.USERPWD, "USERNAME:PASSWORD")
    curl.setopt(pycurl.POST, 1)
    curl.setopt(pycurl.READDATA, body_as_file_object)
    curl.setopt(pycurl.POSTFIELDSIZE, len(body_as_json_string))
    curl.setopt(pycurl.WRITEFUNCTION, lambda x: None)
    print("Sending Data to CESNET.")
    curl.perform()
    result = curl.getinfo(pycurl.RESPONSE_CODE)
    print("RESULT:",result)
  return result

# function used to register metadata at cesnet
# args:
#   cesneturl - the url to the stact catalog
#   json_data - the metadata we wish to update
#   update_id - the product id we wish to update
# returns:
#   status code of the post request
# NOTE: please add the correct username and password to authenticate at cesnet

def update_feature(cesneturl,json_data,update_id):
  result=""
  cesneturl = cesneturl + "/" + update_id
  if json_data !="":
    body_as_json_string = json.dumps(json_data) # dict to json
    body_as_file_object = StringIO(body_as_json_string)
    curl = pycurl.Curl()
    curl.setopt(pycurl.URL, cesneturl)
    curl.setopt(pycurl.HTTPHEADER, ['Accept: application/json','Content-Type: application/json'])
    curl.setopt(pycurl.USERPWD, "USERNAME:PASSWORD")
    curl.setopt(pycurl.PUT, 1)
    curl.setopt(pycurl.READDATA, body_as_file_object)
    curl.setopt(pycurl.WRITEFUNCTION, lambda x: None)
    print("Sending Data to CESNET.")
    curl.perform()
    result = curl.getinfo(pycurl.RESPONSE_CODE)
    print("RESULT:",result)
  return result


# URL for collection at CESNET where products will be registered
cesneturl="https://resto.c-scale.zcu.cz/collections/S2/items"

current_file = 1
skipped_files = []
not_found = []
update_failed = []
new = []
# loop through the product names existing in the list and register them at cesnet
for filename in existingfiles:
  result=""
  print("\nRegistering file ",current_file," out of ", total_products)
  metadata=""
  # metadata=getmetadata(itemdict,filename)
  metadata=getmetadata(items,filename)
  # print(metadata)
  if metadata != "":
    print("PRODUCT",filename)
    composed_filename =filename+".SAFE"
    metadatatoregister=replacementadata(metadata,composed_filename)
    if metadatatoregister != "":
      result=registercesnet(cesneturl,metadatatoregister)
      if str(result) == "409":
        print("\nProduct already registered at CESNET\n")
        update_id=get_id(items,filename)
        result2=update_feature(cesneturl,metadatatoregister,update_id)
        if str(result2) == "200":
          print("\nProduct ",filename,"successfully updated at CESNET\n")
        else:
          print("\nProduct update failed for ",filename,"\n")
          update_failed.append(filename)
      elif str(result) == "200":
        print("\nProduct",filename,"successfully registered at CESNET\n")
        new.append(filename)
    else:
      print("\nFile manifest.safe not found for product ",filename,"\n")
      not_found.append(filename)
  else:
    print("\nProduct Metadata not found for product: ", filename)
    skipped_files.append(filename)
  current_file+=1

print("\nList of new products registered:\n", new)
print("\nList of products skipped (Metadata not found):\n", skipped_files)
print("\nList of products not found (.manifest file not found):\n", not_found)
print("\nList of products update failed:\n", update_failed)