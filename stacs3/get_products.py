#################################################
#                                               #
#               LIP / INCD 2022                 #
#                                               #
#   Authors:                                    #
#           Zacarias Benta - zacarias@lip.pt    # 
#           César Ferreira - cesar@lip.pt       #
#                                               #
#################################################

from eodag.api.core import EODataAccessGateway
from shapely.geometry import shape
from eodag import setup_logging
setup_logging(3)

geom = {}
# Geometry coordinates for the area to search
# Example for the south area of Iberian Peninsula
# geom =  {
#   "type": "Polygon",
#   "coordinates": [
#     [
#       [
#         -7.682155041704681,
#         38.620982842287496
#       ],
#       [
#         -7.682155041704681,
#         36.18203953636458
#       ],
#       [
#         -5.083888440142181,
#         36.18203953636458
#       ],
#       [
#         -5.083888440142181,
#         38.620982842287496
#       ],
#       [
#         -7.682155041704681,
#         38.620982842287496
#       ]
#     ]
#   ]
# }

final_geom = shape(geom)

dag = EODataAccessGateway()

# Product provider. In our case we used creodias
dag.set_preferred_provider("creodias")

# The following for will download products one by one, as it was the best option we found in terms of performance
for i, page_results in enumerate(dag.search_iter_page(
            productType="CHANGE_TO_PRODUCT_TYPE_DESIRED" , # EXAMPLE: "S2_MSI_L1C"
            start="START_DATE", # Date format: yyyy-mm-dd
            end="END_DATE", # Date format: yyyy-mm-dd
            geom=final_geom,
            cloudCover=80,
            items_per_page=1)):
    # Download the product, if it fails it will WAIT 1 minute between each retry and after 5 atempts it will skip the product download
    product_paths= dag.download_all(page_results,wait=1,timeout=5,extract=False)
    print(page_results)
