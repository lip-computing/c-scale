Main repository for C-Scale applications: OpenEO, StacS3, S3Daemonset and Nginx Ingress.

To deploy this C-SCALE APP you need to deploy the following services in the order they are presented:
- S3Daemonset
- StacS3
- Zookeeper
- OpenEO
- Nginx   
